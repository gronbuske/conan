#pragma once
#include "Texture.h"
#include <glm/glm.hpp>

class GameObject
{
public:
	GameObject();
	~GameObject();
	
	Texture2D* texture;
	glm::vec2 position;
	glm::vec2 size;
	GLfloat rotation;
	glm::vec3 color;
};