#pragma once

#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <boost/python.hpp>
#include <Python.h>

enum GameState {
	GAME_ACTIVE,
	GAME_MENU,
	GAME_WIN
};

class Game
{
public:
	// Game state
	GameState              State;
	GLbyte              Keys[1024];
	GLuint                 Width, Height;
	// Constructor/Destructor
	Game(GLuint width, GLuint height);
	~Game();
	// Initialize game state (load all shaders/textures/levels)
	void Init();
	// GameLoop
	void ProcessInput(GLfloat dt);
	void Update(GLfloat dt);
	void Render();

private:
	boost::python::object mainModule;
	boost::python::object mainNamespace;

	void AddGameObject(std::string textureName, glm::vec2 position, glm::vec2 size, float rotation, glm::vec3 color);
};