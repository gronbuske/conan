#include "Game.h"
#include "ResourceManager.h"
#include "SpriteRenderer.h"
#include "GameObject.h"
#include <list>
#include <algorithm>
#include <GLFW/glfw3.h>

SpriteRenderer  *Renderer;
std::list<GameObject*> gameObjects;

using namespace boost::python;

Game::Game(GLuint width, GLuint height)
	: State(GAME_ACTIVE), Keys(), Width(width), Height(height)
{
	Py_Initialize();
	mainModule = import("__main__");
	mainNamespace = mainModule.attr("__dict__");
}

Game::~Game()
{
	Py_Finalize();
	gameObjects.clear();
	delete Renderer;
}

void Game::Init()
{
	// Load shaders
	ResourceManager::LoadShader("Sprite.vert", "Sprite.frag", nullptr, "sprite");
	// Configure shaders
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->Width), static_cast<GLfloat>(this->Height), 0.0f, -1.0f, 1.0f);
	ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
	// Load textures
	ResourceManager::LoadTexture("red.bmp", GL_TRUE, "red");
	// Set render-specific controls
	Renderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));
}

void Game::Update(GLfloat dt)
{
}

void Game::ProcessInput(GLfloat dt)
{
	if (Keys[GLFW_KEY_ENTER] == GLFW_PRESS)
	{
		Keys[GLFW_KEY_ENTER] = GLFW_REPEAT;
		try
		{
			exec_file("scripts/script.py", mainNamespace, mainNamespace);


			int x = extract<int>(mainNamespace["x"]);
			int y = extract<int>(mainNamespace["y"]);

			AddGameObject("red", glm::vec2(x, y), glm::vec2(100, 100), 0.0f, glm::vec3(1, 1, 1));
		}
		catch (error_already_set) {
			PyErr_Print();
		}
	}
}

void Game::Render()
{
	for(GameObject* object : gameObjects)
	{
		Renderer->DrawSprite(*object->texture, object->position, object->size, object->rotation, object->color);
	}
}

void Game::AddGameObject(std::string textureName, glm::vec2 position, glm::vec2 size, float rotation, glm::vec3 color)
{
	auto gameobject = new GameObject();
	gameobject->position = position;
	gameobject->rotation = rotation;
	gameobject->size = size;
	gameobject->color = color;
	gameobject->texture = &ResourceManager::GetTexture(textureName);
	gameObjects.push_back(gameobject);
}
